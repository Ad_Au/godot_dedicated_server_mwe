extends Node

const SERVER_PORT = 6869
const SERVER_IP = "127.0.0.1"

var room_scene = preload("res://Room.tscn")

func _ready():
	get_tree().connect("connected_to_server", self, "_connected_to_server")
	get_tree().connect("server_disconnected", self, "_server_disconnected")

func _on_ConnectBut_pressed():
	var client_peer = NetworkedMultiplayerENet.new()
	var res = client_peer.create_client(SERVER_IP, SERVER_PORT)
	
	match res:
		ERR_CANT_CREATE:
			print("Error: can't create server")
		ERR_ALREADY_IN_USE:
			print("Error: port already in use")
		OK:
			get_tree().set_network_peer(client_peer)
		_:
			print("Unknown error. Error code:", res)

func _connected_to_server():
	var id = get_tree().get_network_unique_id()
	print("Connected with id: ", id)

func _server_disconnected():
	print("Disconnected from server")
	
func client_connected():
	return get_tree().has_network_peer()
	
func send_info():
	rpc_id(1, "_update_info", $NameEdit.text)
	
func _on_DisconnectBut_pressed():
	get_tree().set_network_peer(null)

func _on_RandomBut_pressed():
	rpc_id(1, "_request_random_room")

func _on_PrivateBut_pressed():
	var room_name = $RoomNameEdit.text
	rpc_id(1, "_request_private_room", room_name)

func _on_SendBut_pressed():
	send_info()

remote func _room_joined(room_name):
	print("Room joined: ", room_name)
	
remote func _load_room(room_name):
	print("Load room")
	var scn = room_scene.instance()
	scn.set_name(room_name)
	add_child(scn)
	
remote func _room_full(room_name):
	print("Room full: ", room_name)