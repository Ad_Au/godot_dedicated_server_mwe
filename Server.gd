extends Node

const SERVER_PORT = 6869
const MAX_PLAYERS = 100

var room_scene = preload("res://Room.tscn")
var players = {}

func _ready():
	get_tree().connect("network_peer_connected", self, "_client_connected")
	get_tree().connect("network_peer_disconnected", self, "_client_disconnected")
	var server_peer = NetworkedMultiplayerENet.new()
	var res = server_peer.create_server(SERVER_PORT, MAX_PLAYERS)
	
	match res:
		ERR_CANT_CREATE:
			print("Error: can't create server")
			get_tree().quit()
		ERR_ALREADY_IN_USE:
			print("Error: port already in use")
			get_tree().quit()
		OK:
			get_tree().set_network_peer(server_peer)
		_:
			print("Unknown error. Error code:", res)
			
func _client_connected(id):
	players[id] = {}
	print("New client: ", id)
	print("Number of connected clients: ", players.size())

func _client_disconnected(id):
	players.erase(id)
	print("Lost client: ", id)
	print("Number of connected clients: ", players.size())
	
remote func _request_random_room():
	var sender_id = get_tree().get_rpc_sender_id()
	print("New Request for random room from ", sender_id, " ", players[sender_id][name])
	
	
remote func _request_private_room(room_name):
	var sender_id = get_tree().get_rpc_sender_id()
	print("New Request for private room from ", sender_id," ", players[sender_id][name], " : ", room_name)
	
	if !players.has(sender_id):
		print("Private room requested with unknown sender id: ", sender_id)
		return
		
	if room_name.empty(): # Disallow empty room name
		print("Empty room requested from id: ", sender_id)
		return

	if players[sender_id].has(room_name):
		print("Player ", sender_id, " is already in a room: ", room_name)
		return

	var room_node = get_node(room_name)
	if room_node: # Room exists
		if room_node.is_playing: # Room is full
			print("Room is full")
			rpc_id(sender_id, "_room_full", room_name)
		else: # Room is waiting for second player
			print("Room exists, Joining")
			rpc_id(sender_id, "_room_joined", room_name)
			players[sender_id]["room"] = room_name
			room_node.room_players[sender_id] = players[sender_id]
			for id in room_node.room_players.keys():
				rpc_id(id, "_load_room", room_name)
			room_node.start_game()
	else: # Room doesn't exist, create it
		print("Room doesn't exist, creating")
		var scn = room_scene.instance()
		scn.set_name(room_name)
		add_child(scn)
		rpc_id(sender_id, "_room_joined", room_name)
		players[sender_id]["room"] = room_name
		scn.room_players[sender_id] = players[sender_id]
	
	
remote func _update_info(new_info):
	var sender_id = get_tree().get_rpc_sender_id()
	players[sender_id][name] = new_info
	print("New info from ", sender_id, " Name: ", new_info)