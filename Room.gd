extends Control
	
var is_playing = false

var room_players = {}

func _ready():
	pass
	
func start_game():
	if get_tree().is_network_server():
		var t = Timer.new()
		add_child(t)
		t.connect("timeout", self, "_on_timeout")
		t.start()
	
func _on_timeout():
	print("timeout")
	rpc("new_rotation", $Sprite.rotation + PI/8)
	
remotesync func new_rotation(new):
	$Sprite.rotation = new